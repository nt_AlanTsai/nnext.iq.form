sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.App", {
		onInit: function() {
			var ctrl = this;
			var sViewName = "Leave"; // 未來可改成動態接參數顯示view
			var oContent = ctrl.getView().byId("content");

			new sap.ui.core.mvc.XMLView({
				viewName: "nnext.iq.Form.view." + sViewName
			}).placeAt(oContent);

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("BPM", "FORM_SUBMIT", this.onSumit, this);
		},
		onSumit: function(sChanel, sEvent, oData) {
			MessageToast.show(JSON.stringify(oData));
		},
		onSubmitPress: function() {
			sap.ui.getCore().getEventBus().publish("FORM", "FORM_SUBMIT");
		}
	});
});